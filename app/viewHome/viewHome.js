'use strict';

angular.module('myApp.viewHome', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewHome', {
            templateUrl: 'viewHome/viewHome.html',
            controller: 'ViewHomeCtrl'
        });
    }])

    .controller('ViewHomeCtrl', ['$http', '$rootScope', '$scope','AuthService', function ($http, $rootScope, $scope, AuthService) {
        var URL = 'http://localhost:8080';
        var self = this;
        var numberofworkouts;
        var target;
        var time;
        var diff;
        var procent;
        var gymCounter;
        var daysToEnd;
        var procentofday;
        var username = AuthService.loggedInUser.username;

        this.typeList = [];

        this.loggedInUser = AuthService.loggedInUser.id;

        this.username = AuthService.loggedInUser.username;

        $scope.date = new Date();

        this.formType = {
            'userId': self.loggedInUser,
            'workoutType': ''
        };

        this.sendToBackendType = function () {
            $http.post(URL + "/type/add",self.formType)
                .then(function (data) {
                    console.log(data.str.substring(0, 1).toUpperCase() + str.substring(1));
                    self.fetchType();
                    // window.location.reload()

                }, function (data) {
                    console.log(data)

                })

        };

        this.fetchType = function () {
            $http.get(URL + '/type/list')
                .then(
                    function (data) {
                        console.log(data);
                        var types = data.data.objects;

                        self.typeList = [];
                        for (var index in types) {
                            console.log(types[index])
                            self.typeList.push(types[index]);
                            self.fetchGym(types[index].type);
                        }

                    },
                    function () {
                        console.log("error");

                    }
                );
        };
        self.fetchType();


        this.fetchNumberofWorkouts = function () {
            $http.get(URL + '/extra/numberofworkouts')
                .then(
                    function (data) {
                        self.numberofworkouts = data.data;
                        console.log(data);
                    },
                    function () {
                        console.log("error")

                    }
                )

        };
        self.fetchNumberofWorkouts();

        this.fetchTarget = function () {
            $http.get(URL + '/extra/target')
                .then(
                    function (data) {
                        self.target = data.data;
                        console.log(data);
                    },
                    function () {
                        console.log("error")

                    }
                )

        };
        self.fetchTarget();

        this.fetchTime = function () {
            $http.get(URL + '/extra/time')
                .then(
                    function (data) {
                        self.time = data.data;
                        console.log(data);
                    },
                    function () {
                        console.log("error")

                    }
                )

        };
        self.fetchTime();

        this.fetchDiff = function () {
            $http.get(URL + '/extra/current?userId='+self.loggedInUser)
                .then(
                    function (data) {
                        self.diff = data.data;
                        console.log(data);
                        self.procent = 100 - Math.abs(self.diff);

                    },
                    function () {
                        console.log("error")

                    }
                )

        };
        self.fetchDiff();

        this.fetchDaysToEnd = function () {
            $http.get(URL + '/extra/daystoend')
                .then(
                    function (data) {
                        self.daysToEnd = data.data;
                        console.log(data);
                        self.procentoday = 100 - Math.abs(self.daysToEnd);
                    },
                    function () {
                        console.log("error")

                    }
                )

        };
        self.fetchDaysToEnd();

        self.counters = {};
        this.fetchGym = function (type) {
            if(type === undefined){
                return;
            }
            $http.get(URL + '/extra/typecounter?type=' + type + "&userId=" + self.loggedInUser)
                .then(
                    function (data) {
                        console.log(data.data);
                        self.counters[type] = data.data;
                    },
                    function () {
                        console.log("error")

                    }
                )

        };
        self.fetchGym();




    }]);