'use strict';

angular.module('myApp.viewWeightTarget', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/viewWeightTarget', {
    templateUrl: 'viewWeightTarget/viewWeightTarget.html',
    controller: 'ViewWeightTargetCtrl'
  });
}])

.controller('ViewWeightTargetCtrl', ['$http', '$rootScope','AuthService',function($http, $rootScope, AuthService) {
    var URL = 'http://localhost:8080';
    var self = this;

    this.progressList = [];

    this.dateTarget = "";

    var userId = AuthService.loggedInUser.id;


    this.formWeightTarget = {
        'userId': userId,
        'weightTarget': '',
        'dateTarget': ''
    };

    // this.formProgress = {
    //     'userId': userId,
    //     'weight': '',
    //     'date': ''
    // };

    this.sendToBackend = function () {
        self.formWeightTarget.dateTarget = document.getElementById('dateTarget').value;
      $http.post(URL + "/target/add",self.formWeightTarget)
          .then(function (data) {
              console.log(data)
              window.location = "#!/viewHome";

          }, function (data) {
              console.log(data)

          })


    };

    // this.sendToBackendProgress = function () {
    //     $http.post(URL + "/progress/add",self.formProgress)
    //         .then(function (data) {
    //             console.log(data)
    //             self.fetchProgress();
    //         }, function (data) {
    //             console.log(data)
    //
    //         })
    //
    // };
    //
    //
    //
    // this.fetchProgress = function () {
    //     $http.get(URL + '/progress/listall')
    //         .then (
    //             function (data) {
    //                 console.log(data);
    //                 var progresses = data.data.objects;
    //
    //                 self.progressList = [];
    //                 for (var index in progresses) {
    //                     console.log(progresses[index]);
    //                     self.progressList.push(progresses[index]);
    //                 }
    //
    //             },
    //             function () {
    //                 console.log("error");
    //
    //             }
    //         );
    // };
    // self.fetchProgress();



}]);