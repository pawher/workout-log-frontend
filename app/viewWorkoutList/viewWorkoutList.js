'use strict';

angular.module('myApp.viewWorkoutList', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewWorkoutList', {
            templateUrl: 'viewWorkoutList/viewWorkoutList.html',
            controller: 'viewWorkoutListCtrl'
        });
    }])

    .controller('viewWorkoutListCtrl', ['$http', '$rootScope', function ($http, $rootScope) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.workoutList = [];
        this.loggedInUser = $rootScope.loggedInUser;
        this.currentPage = 0;
        this.totalPages = 0;


        this.fetchWorkout = function (page_number) {
            $http.get(URL + '/workout/page?page=' + page_number)
                .then(
                    function (data) {
                        console.log(data);
                        var workouts = data.data.objects;

                        self.workoutList = [];

                        for (var index in workouts) {
                            console.log(workouts[index]);
                            self.workoutList.push(workouts[index]);

                        }
                        self.currentPage = data.data.currentPage;
                        self.totalPages = data.data.totalElements;
                    },
                    function () {
                        console.log("error");
                    }
                );
        };


        this.removeWorkout = function (id_removed) {

            $http.delete(URL + '/workout/remove?id=' + id_removed)
                .then(function (data) {
                    console.log(data);
                    location.reload();


                }, function (data) {
                    console.log(data)

                })

        };


        this.fetchNext = function () {
            if (self.currentPage < self.totalPages) {
                self.fetchWorkout(self.currentPage + 1);
            }
        };

        this.fetchPrevious = function () {
            if (self.currentPage == 0) {
                return;
            }
            self.fetchWorkout(self.currentPage - 1);
        };

        self.fetchWorkout(self.currentPage);



    }]);