'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.viewHome',
    'myApp.view1',
    'myApp.view2',
    'myApp.viewWorkout',
    'myApp.viewWorkoutList',
    'myApp.viewWeightTarget',
    'myApp.viewProgress',
    'myApp.viewLogin',
    'myApp.viewBMI',
    'myApp.authService',
    'myApp.version'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/viewLogin'});
}])
    .run(function ($rootScope, AuthService, $window, $http) {

        if (AuthService.loggedInUser.id === '') { // nie jest zalogowany
            var token = $window.sessionStorage.getItem('token');
            var user_id = $window.sessionStorage.getItem('user_id');

            if (token !== null) {
                $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                AuthService.logged_in_id = user_id;
                $rootScope.loggedIn = true;
            } else {
                $rootScope.loggedIn = false;
            }
        } else {
            $rootScope.loggedIn = true;
        }


    });
