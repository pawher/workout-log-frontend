'use strict';

angular.module('myApp.viewBMI', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/viewBMI', {
    templateUrl: 'viewBMI/viewBMI.html',
    controller: 'viewBMICtrl'
  });
}])

.controller('viewBMICtrl', ['$http', '$rootScope','$scope', function($http, $rootScope,$scope) {
    var URL = 'http://localhost:8080';
    var self = this;
    var nan = "NaN";

    this.weight;
    this.height;
    this.BMI;
    this.nan;


    this.progressList = [];

    var userId = $rootScope.loggedInUser;

    this.calculateBMI = function () {
        self.BMI = ((self.weight)/((self.height/100)*(self.height/100))).toPrecision(4);
            // alert(self.BMI).set('basic', false);

        console.log(self.BMI)

    };

    // $scope.myFunctionX = function() {
    //     console.log("WTF?");
    //     document.getElementById("demo").innerHTML = "YOU CLICKED ME!";
    // };


}]);