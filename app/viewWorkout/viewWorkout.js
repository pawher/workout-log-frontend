'use strict';

angular.module('myApp.viewWorkout', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/viewWorkout', {
    templateUrl: 'viewWorkout/viewWorkout.html',
    controller: 'ViewWorkoutCtrl'
  });
}])

.controller('ViewWorkoutCtrl', ['$http', '$rootScope', 'AuthService',function($http, $rootScope, AuthService) {
  var URL = 'http://localhost:8080';
  var self = this;

  var userId = AuthService.loggedInUser.id;

  this.loggedInUser = AuthService.loggedInUser;

  this.typeList = [];

  this.workoutDate = '';
  this.formWorkout = {
      'userId': userId,
      'description': '',
      'workoutDate': '',
      'time': '',
      'workoutType': ''
  };


  this.sendToBackend = function () {
      self.formWorkout.workoutDate = document.getElementById( 'workoutDate' ).value;
      console.log(document.getElementById( 'workoutDate' ).value);
      $http.post(URL + "/workout/add", self.formWorkout)
          .then(function (data) {
              console.log(data)
              window.location = "#!/viewWorkoutList";
          }, function (data) {
              console.log(data)

          })

  }

    this.fetchType = function () {
        $http.get(URL + '/type/list')
            .then (
                function (data) {
                    console.log(data);
                    var types = data.data.objects;

                    self.typeList = [];
                    for (var index in types) {
                        console.log(types[index])
                        self.typeList.push(types[index]);
                    }

                },
                function () {
                    console.log("error");

                }
            );
    };
  self.fetchType();



}]);